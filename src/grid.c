#include "grid.h"

#include <string.h>
#include <assert.h>
#include "c_math.h"
#include "bbox.h"

void 
alloc_grid(GridAlloc *ga, uint x, uint y, uint z)
{
    ga->g = alloc_transMem(ga->grid_tm, x * y * z);

    ga->gi.x = x;
    ga->gi.y = y;
    ga->gi.z = z;

    ga->rows = alloc_transMem(ga->rows_tm, GRID_ROW_SIZE);
    ga->r_count = 1;
    ga->r_last  = 0;
}

void
new_grid(GridAlloc *ga, flo3 a, flo3 b, flo3 dens)
{
    flo3 grid_size = b;
    sub3(&grid_size, &a);

    flo3 counts = mkflo3(30); // magic constant (TODO)
    div3(&counts, &dens);
    
    MAP(counts.s, floorf, 3, counts.s);

    alloc_grid(ga, counts.s[0], counts.s[1], counts.s[2]);
    ga->gi.box.a = a;
    ga->gi.box.b = b;

    ga->gi.csize = grid_size;
    div3(&ga->gi.csize, &counts);
}

void 
delete_grid(GridAlloc *ga)
{
    ga->r_count = ga->r_last = 0;
    ga->gi.x = ga->gi.y = ga->gi.z = 0;
}

uint
alloc_row(GridAlloc *ga)
{
    ga->r_count++;
    ga->r_last++;
    void *p = alloc_transMem(ga->rows_tm, GRID_ROW_SIZE);
    bzero(p, GRID_ROW_SIZE * sizeof(GridRow));
    ga->rows = ga->rows_tm->mem;
    return ga->r_last;
}

void
insert_into_cell(GridAlloc *ga, uint cell, uint value)
{
    if (cell > (uint)(ga->gi.x * ga->gi.y * ga->gi.z))
        return;

    if (0 == ga->g[cell])
    {
        ga->g[cell] = alloc_row(ga);
        set_changed_item_transMem(ga->grid_tm, cell);
    }

    uint current_row = ga->g[cell];
    while (1)
    {
        for (uint i = 1; i < GRID_ROW_SIZE; i++)
            if (0 == GRID_ROW(ga->rows, current_row, i) ||
                    value == GRID_ROW(ga->rows, current_row, i))
            {
                GRID_ROW(ga->rows, current_row, i) = value;
                set_changed_item_transMem(ga->rows_tm, 
                        current_row * GRID_ROW_SIZE + i);
                return;
            }

        if (0 == GRID_ROW(ga->rows, current_row, 0))
        {
            GRID_ROW(ga->rows, current_row, 0) = alloc_row(ga);
            set_changed_item_transMem(ga->rows_tm, current_row * GRID_ROW_SIZE);
        }

        current_row = GRID_ROW(ga->rows, current_row, 0);
    }
}

void /* outdate */
delete_from_cell(GridAlloc *ga, uint cell, uint value)
{
    uint current_row = ga->g[cell];
    while (current_row)
    {
        for (uint i = 1; i < GRID_ROW_SIZE; i++)
            if (value == GRID_ROW(ga->rows, current_row, i))
            {
                GRID_ROW(ga->rows, current_row, i) = 0;
                return;
            }
        current_row = GRID_ROW(ga->rows, current_row, 0);
    }
}

#define FOR_BOX(box, gi)                                \
    do                                                  \
    {                                                   \
        const int x_st  = GET_GRID_X(box.a, gi)-0;        \
        const int y_st  = GET_GRID_Y(box.a, gi)-0;        \
        const int z_st  = GET_GRID_Z(box.a, gi)-0;        \
        const int x_end = GET_GRID_X(box.b, gi)+0;        \
        const int y_end = GET_GRID_Y(box.b, gi)+0;        \
        const int z_end = GET_GRID_Z(box.b, gi)+0;        \
        for (int x = _my_max(0, x_st);                  \
                x <= _my_min(gi.x-1, x_end); x++)       \
        for (int y = _my_max(0, y_st);                  \
                y <= _my_min(gi.y-1, y_end); y++)       \
        for (int z = _my_max(0, z_st);                  \
                z <= _my_min(gi.z-1, z_end); z++)       \
        {

#define ENDFOR_BOX(box, gi)                             \
        }                                               \
    } while (0)


void
insert_into_grid(GridAlloc *ga, BBox *box, uint value)
{
    FOR_BOX((*box), ga->gi);
        int index = GET_GRID_CELL_(x, y, z, ga->gi);
        insert_into_cell(ga, index, value);
    ENDFOR_BOX((*box), ga->gi);
}

void
delete_from_grid(GridAlloc *ga, BBox *box, uint value)
{
    FOR_BOX((*box), ga->gi);
        int index = GET_GRID_CELL_(x, y, z, ga->gi);
        delete_from_cell(ga, index, value);
    ENDFOR_BOX((*box), ga->gi);
}

void
update_box_in_grid(GridAlloc *ga, BBox *old, BBox *new_, uint value)
{
    delete_from_grid(ga, old, value);
    insert_into_grid(ga, new_, value);
}

void
insert_sphere_into_grid(GridAlloc *ga, ObjSphere *obj, uint value)
{
    BBox box;
    sphere_bbox(obj, &box);
    insert_into_grid(ga, &box, value);
}

void
insert_tri_into_grid(GridAlloc *ga, ObjTriangle *obj, uint value)
{
    BBox box;
    tri_bbox(obj, &box);
    insert_into_grid(ga, &box, value);
}

void
insert_plane_into_grid(GridAlloc *ga, ObjPlane *obj, uint value)
{
    BBox box;
    plane_bbox(obj, &box);
    insert_into_grid(ga, &box, value);
}

void
insert_obj_into_grid(GridAlloc *ga, Obj *obj, uint value)
{
    switch (obj->type)
    {
    case OBJ_TRIANGLE: insert_tri_into_grid(ga,    &obj->d.tri,    value); break;
    case OBJ_PLANE:    insert_plane_into_grid(ga,  &obj->d.plane,  value); break;
    case OBJ_SPHERE:   insert_sphere_into_grid(ga, &obj->d.sphere, value); break;
    }
}

void
insert_objs_into_grid(GridAlloc *ga, Obj *objs, uint num)
{
    for (uint i = 0; i < num; i++)
        insert_obj_into_grid(ga, &objs[i], i + 1);
}

