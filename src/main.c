#include <stdio.h>
#include <ctype.h>
#include <SDL2/SDL.h>

#include "shared.h"
#include "c_math.h"
#include "grid.h"
#include "transmem.h"
#include "scene.h"
 
#define CL_CHECK(_expr)                                                         \
   do {                                                                         \
     cl_int _err = _expr;                                                       \
     if (_err == CL_SUCCESS)                                                    \
       break;                                                                   \
     fprintf(stderr, "OpenCL Error: '%s' in function '%s' "                     \
                     "(line %d, file %s) have value %d!\n",                     \
                     #_expr, __FUNCTION__, __LINE__, __FILE__, (int)_err);      \
     abort();                                                                   \
   } while (0)

cl_context       context;
cl_command_queue queue;
cl_program       program;
cl_device_id     device;
cl_kernel        kernel;

cl_mem           pixels_mem;

void pfn_notify(const char *errinfo, const void *private_info, size_t cb, void *user_data)
{
	fprintf(stderr, "OpenCL Error (via pfn_notify): %s\n", errinfo);
}

void
opencl_init(int type)
{
    puts("OpenCL");
    cl_platform_id platforms[100];
    cl_uint platforms_n = 0;
    CL_CHECK(clGetPlatformIDs(100, platforms, &platforms_n));

    printf("=== %d OpenCL platform(s) found: ===\n", platforms_n);
    for (int i=0; i<platforms_n; i++)
    {
        char buffer[10240];
        printf("  -- %d --\n", i);
        CL_CHECK(clGetPlatformInfo(platforms[i], CL_PLATFORM_PROFILE, 10240, buffer, NULL));
        printf("  PROFILE = %s\n", buffer);
        CL_CHECK(clGetPlatformInfo(platforms[i], CL_PLATFORM_VERSION, 10240, buffer, NULL));
        printf("  VERSION = %s\n", buffer);
        CL_CHECK(clGetPlatformInfo(platforms[i], CL_PLATFORM_NAME, 10240, buffer, NULL));
        printf("  NAME = %s\n", buffer);
        CL_CHECK(clGetPlatformInfo(platforms[i], CL_PLATFORM_VENDOR, 10240, buffer, NULL));
        printf("  VENDOR = %s\n", buffer);
        CL_CHECK(clGetPlatformInfo(platforms[i], CL_PLATFORM_EXTENSIONS, 10240, buffer, NULL));
        printf("  EXTENSIONS = %s\n", buffer);
    }

    if (platforms_n == 0)
        return;

    cl_device_id devices[100];
    cl_uint devices_n = 0;
    // CL_DEVICE_TYPE_{ALL,CPU,GPU}
    CL_CHECK(clGetDeviceIDs(platforms[0], type, 100, devices, &devices_n));

    printf("=== %d OpenCL device(s) found on platform:\n", platforms_n);
    for (int i=0; i<devices_n; i++)
    {
        char buffer[10240];
        cl_uint buf_uint;
        cl_ulong buf_ulong;
        printf("  -- %d --\n", i);
        CL_CHECK(clGetDeviceInfo(devices[i], CL_DEVICE_NAME, sizeof(buffer), buffer, NULL));
        printf("  DEVICE_NAME = %s\n", buffer);
        CL_CHECK(clGetDeviceInfo(devices[i], CL_DEVICE_VENDOR, sizeof(buffer), buffer, NULL));
        printf("  DEVICE_VENDOR = %s\n", buffer);
        CL_CHECK(clGetDeviceInfo(devices[i], CL_DEVICE_VERSION, sizeof(buffer), buffer, NULL));
        printf("  DEVICE_VERSION = %s\n", buffer);
        CL_CHECK(clGetDeviceInfo(devices[i], CL_DRIVER_VERSION, sizeof(buffer), buffer, NULL));
        printf("  DRIVER_VERSION = %s\n", buffer);
        CL_CHECK(clGetDeviceInfo(devices[i], CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(buf_uint), &buf_uint, NULL));
        printf("  DEVICE_MAX_COMPUTE_UNITS = %u\n", (unsigned int)buf_uint);
        CL_CHECK(clGetDeviceInfo(devices[i], CL_DEVICE_MAX_CLOCK_FREQUENCY, sizeof(buf_uint), &buf_uint, NULL));
        printf("  DEVICE_MAX_CLOCK_FREQUENCY = %u\n", (unsigned int)buf_uint);
        CL_CHECK(clGetDeviceInfo(devices[i], CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(buf_ulong), &buf_ulong, NULL));
        printf("  DEVICE_GLOBAL_MEM_SIZE = %llu\n", (unsigned long long)buf_ulong);
    }

    // init
    context = clCreateContext(NULL, 1, devices, &pfn_notify, NULL, NULL);
    queue   = clCreateCommandQueue(context, devices[0], 0, NULL);
    device  = devices[0];
}

void load_program()
{
    cl_int ret;
    char *source_str;
    size_t source_size;
    FILE *fp = fopen("compiled.cl", "r");
    if (!fp) return;

    const int max_source_size = 1024 * 1024 * 1024;
    source_str = malloc(max_source_size);
    source_size = fread(source_str, 1, max_source_size, fp);
    fclose(fp);

    program = clCreateProgramWithSource(context, 1,
            (const char **) &source_str, (const size_t*) &source_size, &ret);
    ret = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);

    if (ret != CL_SUCCESS)
    {
        // check build error and build status first
        cl_build_status status;
        clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_STATUS,
                sizeof(cl_build_status), &status, NULL);

        // check build log
        size_t logSize = source_size;
        clGetProgramBuildInfo(program, device,
                CL_PROGRAM_BUILD_LOG, 0, NULL, &logSize);
        char *programLog = calloc(logSize+1, sizeof(char));
        clGetProgramBuildInfo(program, device,
                CL_PROGRAM_BUILD_LOG, logSize+1, programLog, NULL);
        printf("Build failed; error=%d, status=%d, programLog:\n\n%s",
                ret, status, programLog);
        free(programLog);
        exit(1);
    }

    kernel = clCreateKernel(program, "render", &ret);
    if (ret != CL_SUCCESS)
    {
        printf("bad clCreateKernel\n");
    }
    else
        printf("Kernel 'compiled.cl' is loaded\n");
}

void prepare_mem(SceneInfo *si, int w, int h)
{
    new_transMem(&si->transmem[SC_Objects],   TM_RO, sizeof(Obj));
    new_transMem(&si->transmem[SC_Lights],    TM_RO, sizeof(Lig));
    new_transMem(&si->transmem[SC_Materials], TM_RO, sizeof(Mat));
    new_transMem(&si->transmem[SC_Grid],      TM_RO, sizeof(Grid));
    new_transMem(&si->transmem[SC_GridRows],  TM_RO, sizeof(GridRow));
    new_transMem(&si->transmem[SC_Pixels],    TM_WO, sizeof(pixel));

    alloc_transMem(&si->transmem[SC_Pixels], w * h);
}

void mem_to(SceneInfo *si)
{
    for (uint i = 0; i < SC_LastOne; i++)
        sync_to_cl_transMem(&si->transmem[i], context, queue);

    CL_CHECK(clFlush(queue));
}

void render(int wp, int hp, SceneInfo *si)
{
    size_t globalWrkSize[2];
    globalWrkSize[0] = hp;
    globalWrkSize[1] = wp;

    /*
    size_t localWrkSize[2];
    localWrkSize[0]  = 4;
    localWrkSize[1]  = 4;
    */

    /*
       kernel void render(
           __global Lig *lights,
           __global Mat *materials,
           __global Obj *objects,
           __global Grid *grid,
           __global GridRow *grid_rows,
           uint num_ligs,
           uint num_mats,
           uint num_objs,
           uint num_cells,
           uint num_rows,
           GridInfo gi,
           flo2 view_angle,
           Ray base_ray,
           __global pixel *p)
   */

    int n = 0;
    clSetKernelArg(kernel, n++, sizeof(cl_mem), &si->transmem[SC_Lights].cmem);
    clSetKernelArg(kernel, n++, sizeof(cl_mem), &si->transmem[SC_Materials].cmem);
    clSetKernelArg(kernel, n++, sizeof(cl_mem), &si->transmem[SC_Objects].cmem);
    clSetKernelArg(kernel, n++, sizeof(cl_mem), &si->transmem[SC_Grid].cmem);
    clSetKernelArg(kernel, n++, sizeof(cl_mem), &si->transmem[SC_GridRows].cmem);
    clSetKernelArg(kernel, n++, sizeof(uint),   &si->transmem[SC_Lights].count);
    clSetKernelArg(kernel, n++, sizeof(uint),   &si->transmem[SC_Materials].count);
    clSetKernelArg(kernel, n++, sizeof(uint),   &si->transmem[SC_Objects].count);
    clSetKernelArg(kernel, n++, sizeof(uint),   &si->transmem[SC_Grid].count);
    clSetKernelArg(kernel, n++, sizeof(uint),   &si->transmem[SC_GridRows].count);
    clSetKernelArg(kernel, n++, sizeof(GridInfo), &si->scene.gi);
    clSetKernelArg(kernel, n++, sizeof(Ray),    &si->scene.base_ray);
    clSetKernelArg(kernel, n++, sizeof(cl_mem), &si->transmem[SC_Pixels].cmem);


    CL_CHECK(clEnqueueNDRangeKernel(queue, kernel, 2, NULL, 
                globalWrkSize, NULL, 0, NULL, NULL));
    CL_CHECK(clFinish(queue));
}

void mem_back(SceneInfo *si)
{
    for (uint i = 0; i < SC_LastOne; i++)
        sync_from_cl_transMem(&si->transmem[i], context, queue);

    CL_CHECK(clFinish(queue));
}

void free_mem(SceneInfo *si)
{
    for (uint i = 0; i < SC_LastOne; i++)
        delete_transMem(&si->transmem[i]);
}
    

int main(int argc, char **argv)
{
    /*
     * Init OpenCL & Scene
     */
    int wp = 640;
    if (argc >= 2)
        wp = atoi(argv[1]);
    int hp = wp * 9 / 16;
    if (hp < 1)
        hp = 1;

    SceneInfo si;
    //si.scene.view_angle = (flo2){   
    prepare_mem(&si, wp, hp);

    GridAlloc grid = 
    {
        .grid_tm = &si.transmem[SC_Grid],
        .rows_tm = &si.transmem[SC_GridRows],
    };
    new_grid(&grid, 
            mkflo3(-300),   // a (lowest corner)
            mkflo3(300),    // b (higest corner)
            mkflo3(0.3));     // density
    si.scene.gi = grid.gi;
    SHOWn("%u %u %u", grid.gi.x, grid.gi.y, grid.gi.z);
    SHOW_flo3n(grid.gi.csize);
    SHOW_flo3n(grid.gi.box.a);
    SHOW_flo3n(grid.gi.box.b);

    make_scene(&si);
    insert_objs_into_grid(&grid, 
            si.transmem[SC_Objects].mem, si.transmem[SC_Objects].count);

    if (argc >= 3)
        switch (tolower(argv[2][0]))
        {
        case 'g': opencl_init(CL_DEVICE_TYPE_GPU); break;
        case 'c': opencl_init(CL_DEVICE_TYPE_CPU); break;
        case 'a': opencl_init(CL_DEVICE_TYPE_ALL); break;
        default:  opencl_init(CL_DEVICE_TYPE_ALL);
        }
    else
        opencl_init(CL_DEVICE_TYPE_ALL);

    load_program();
    

    /*
     * Init window (SDL)
     */
    SDL_Window *window;
    SDL_Renderer *renderer;
    SDL_Init(SDL_INIT_VIDEO); 
    SDL_Event event;
    window   = SDL_CreateWindow("CL Ray Tracer", 0, 0, wp, hp, 0);
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_SOFTWARE);

    SDL_Texture *texture = SDL_CreateTexture(renderer,
            SDL_PIXELFORMAT_ARGB8888,
            SDL_TEXTUREACCESS_STREAMING,
            wp, hp);

    SDL_RenderClear(renderer);
    SDL_RenderCopy(renderer, texture, NULL, NULL);
    SDL_RenderPresent(renderer);


    /*
     * Cycle
     */
    char quit = 0;
    char pause = 0;
    flo3 eye = {0, 0 , 0};
    flo angle = -1;
    while (!quit) 
    {
        flo3 lookat = {
            eye.s[0] - 10 * cosf(angle),
            eye.s[1],
            eye.s[2] - 10 * sinf(angle),
        };

        flo3 step = lookat;
        sub3(&step, &eye);
        flo3 diver = mkflo3(1.f / 3.f);
        mul3(&step, &diver);


        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
            case SDL_KEYDOWN:
                switch (event.key.keysym.scancode)
                {
                case SDL_SCANCODE_UP:    sub3(&eye, &step);  break;
                case SDL_SCANCODE_DOWN:  add3(&eye, &step);  break;
                case SDL_SCANCODE_LEFT:  angle += 0.03;  break;
                case SDL_SCANCODE_RIGHT: angle -= 0.03;  break;
                case SDL_SCANCODE_P:     pause = !pause; break;
                default: break;
                }
                break;
            case SDL_QUIT: quit = 1; break;
            default: break;
            }
        }
        if (pause)
        {
            SDL_Delay(1000);
            continue;
        }

        uint start = SDL_GetTicks();


        si.scene.base_ray.d = lookat;
        si.scene.base_ray.o = eye;
        mem_to(&si);
        uint after_mem_to = SDL_GetTicks();
        
        render(wp, hp, &si);
        uint after_ray  = SDL_GetTicks();

        mem_back(&si);
        uint after_mem_back  = SDL_GetTicks();

        SDL_UpdateTexture(texture, NULL, si.transmem[SC_Pixels].mem, wp * sizeof(pixel));
        SDL_RenderCopy(renderer, texture, NULL, NULL);
        SDL_RenderPresent(renderer);
        uint after_render  = SDL_GetTicks();


        int delay = 1000.0f/20 - (after_render - start);
        printf("mem to: %2d, render: %3d, mem back: %d, draw: %d, delay: %2d\n", 
                after_mem_to - start, 
                after_ray - after_mem_to,
                after_mem_back - after_ray,
                after_render - after_mem_back, 
                delay);

        if (delay > 0) SDL_Delay(delay);
    }

    free_mem(&si);
    SDL_Quit();

    return 0;
}

