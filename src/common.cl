#ifndef __COMMON_CL__
#define __COMMON_CL__

#include "shared.h"

#define EPS 1e-4f
#define HIT_FUNCTION hit_grid

typedef struct
{
    flo3 ray_d;
    flo3 normal;
    flo3 hit_point;
    cl_int obj_id;
} HitRec;

// vim: ts=4 sw=4 et ft=c
#endif
