#ifndef __RAY_CL__
#define __RAY_CL__

#pragma OPENCL EXTENSION cl_amd_printf : enable

#include "common.cl"
#include "light_material.cl"
#include "grid.cl"

/*
constant uint colors_count = 6;
constant uchar4 colors[] =
    {
        {0xff, 0x40, 0x40, 0xff},
        {0x40, 0xff, 0x40, 0xff},
        {0x40, 0x40, 0xff, 0xff},
        {0xff, 0xff, 0x40, 0xff},
        {0xff, 0x40, 0xff, 0xff},
        {0x40, 0xff, 0xff, 0xff},
    };
*/

flo3 ray_direction(flo2 p, flo d, flo3 w, flo3 u, flo3 v)
{
    return normalize(p.x * u + p.y * v - d * w);
}

kernel void render(
        __global Lig *lights,
        __global Mat *materials,
        __global Obj *objects,
        __global Grid *grid,
        __global GridRow *grid_rows,
        uint num_ligs,
        uint num_mats,
        uint num_objs,
        uint num_cells,
        uint num_rows,
        GridInfo gi,
        Ray base_ray,
        //flo2 view_angle,
        __global pixel *p)
{
    uint hp = get_global_size(0);
    uint wp = get_global_size(1);
    uint r  = get_global_id(0);
    uint c  = get_global_id(1);

    Scene scene = { 
        .lights   = lights,
        .materials= materials,
        .objects  = objects,
        .grid     = grid,
        .grid_rows= grid_rows,
        .num_ligs = num_ligs,
        .num_mats = num_mats,
        .num_objs = num_objs,
        .gi       = gi,
        .base_ray = base_ray
    };

    flo3 eye    = base_ray.o;
    flo3 lookat = base_ray.d;
    flo3 w = normalize(eye - lookat);
    flo3 u = normalize(cross((flo3)(0, 1, 0), w));
    flo3 v = cross(w, u);

    Ray rs;
    rs.o = scene.base_ray.o;
    flo2 dir = (flo2)(c - 0.5f * (wp - 1), r - 0.5f * (hp - 1));
    rs.d = ray_direction(dir, -300, w, u, v);

    HitRec hit;
    pixel out = 0;

    if (HIT_FUNCTION(&scene, rs, &hit))
    {
        uint i = 1;
        flo3 color = mat_shade(&scene, &hit);

        /*
        rs.d = normalize(src + (flo3)(0.1, 0.5, 0));
        if (HIT_FUNCTION(&scene, rs, &hit))
        {
            color += mat_shade(&scene, &hit);
        }
        i++;
        /*  */

        color *= 255.0f / i;
        if (color.x > 255) color.x = 255;
        if (color.y > 255) color.y = 255;
        if (color.z > 255) color.z = 255;
        out = as_uint((uchar3)(color.z, color.y, color.x));
    }

    p[(hp - 1 - r) * wp + c] = out;
}


// vim: ts=4 sw=4 et ft=c
#endif
