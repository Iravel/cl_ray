#include "scene.h"

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include "c_math.h"


void set_tri(ObjTriangle *tri, flo3 a, flo3 b, flo3 c)
{
    flo u1 = (a.s[0] - b.s[0]), v1 = (a.s[0] - c.s[0]), 
        u2 = (a.s[1] - b.s[1]), v2 = (a.s[1] - c.s[1]), 
        u3 = (a.s[2] - b.s[2]), v3 = (a.s[2] - c.s[2]);

    flo3 norm = {
        u2*v3 - u3*v2,
        u3*v1 - u1*v3,
        u1*v2 - u2*v1
    };
    flo len = -sqrt(norm.s[0]*norm.s[0] + norm.s[1]*norm.s[1] + norm.s[2]*norm.s[2]);

    tri->x = (flo4){a.s[0], b.s[0], c.s[0], norm.s[0]/len};
    tri->y = (flo4){a.s[1], b.s[1], c.s[1], norm.s[1]/len};
    tri->z = (flo4){a.s[2], b.s[2], c.s[2], norm.s[2]/len};
}


int add_triangle(SceneInfo *si, 
        int mat_id,
        flo3 a, flo3 b, flo3 c)
{
    Obj *o = alloc_transMem(&si->transmem[SC_Objects], 1);
    o->type = OBJ_TRIANGLE;
    o->mat_id = mat_id;
    set_tri(&o->d.tri, a, b, c);
    return o - (Obj *)si->transmem[SC_Objects].mem;
}

void add_poligon(SceneInfo *si,
        int mat_id,
        flo3 vertex[], int count)
{
    for (int i = 2; i < count; i++)
        add_triangle(si, mat_id,
                vertex[0], vertex[i-1], vertex[i]);
}
        
void load_ply(Ply *ply, const char *filename)
{
    FILE *in = fopen(filename, "r");
    assert(in != NULL);

    int vertex_props = 0, face_props = 0;
    int cur_head = 0; // 1 -- vertex, 2 -- face
    do
    {
        char word[128];
        char line[128];
        fscanf(in, "%128s", word);

        if (0 == strcmp("end_header", word))
            break;
        else if (0 == strcmp("format", word)
                || 0 == strcmp("comment", word))
            fgets(line, 128, in);
        else if (0 == strcmp("element", word))
        {
            fscanf(in, "%128s", word);
            if (0 == strcmp("vertex", word))
            {
                cur_head = 1;
                fscanf(in, "%d", &ply->v_count);
            }
            else if (0 == strcmp("face", word))
            {
                cur_head = 2;
                fscanf(in, "%d", &ply->f_count);
            }
        }
        else if (0 == strcmp("property", word))
        {
            fgets(&line, 128, in);
            if (1 == cur_head)
                vertex_props++;
            else if (2 == cur_head)
                face_props++;
        }
    } while (1);

    ply->vertexes = calloc(ply->v_count, sizeof(*ply->vertexes));
    ply->faces    = calloc(ply->f_count, sizeof(*ply->faces));

    flo3 *v = ply->vertexes;
    for (int i = 0; i < ply->v_count; i++)
    {
        fscanf(in, "%f %f %f", &v[i].s[0], &v[i].s[1], &v[i].s[2]);
        flo t;
        for (int q = 3; q < vertex_props; q++)
            fscanf(in, "%f", &t);
    }

    for (int i = 0; i < ply->f_count; i++)
    {
        int num;
        fscanf(in, "%d", &num);
        ply->faces[i] = calloc(num + 1, sizeof(**ply->faces));
        for (int j = 0; j < num; j++)
            fscanf(in, "%d", &ply->faces[i][j]);
        ply->faces[i][num] = 0;
    }
}

void add_ply(SceneInfo *si, int mat_id, Ply *ply, flo3 mul, flo3 add)
{
    assert(si != NULL);
    assert(ply != NULL);
    assert(ply->vertexes != NULL);
    assert(ply->faces != NULL);

    for (int i = 0; i < ply->f_count; i++)
    {
        flo3 a = ply->vertexes[ ply->faces[i][0] ];
        mul3(&a, &mul);
        add3(&a, &add);

        for (int j = 2; ply->faces[i][j] != 0; j++)
        {
            flo3 b = ply->vertexes[ ply->faces[i][j - 1] ];
            flo3 c = ply->vertexes[ ply->faces[i][j] ];
            mul3(&b, &mul);
            mul3(&c, &mul);
            add3(&b, &add);
            add3(&c, &add);
            add_triangle(si, mat_id, a, b, c);
        }
    }
}



void make_scene(SceneInfo *si)
{
    Scene *s = &si->scene;
    /*
     * Lights
     */
    Lig *l;
    l = alloc_transMem(&si->transmem[SC_Lights], 1);
    l->type = LIG_AMBIENT;
    l->d.ambient.color = (flo3){1, 1, 1};
    l->d.ambient.ls = 0.2;

    l = alloc_transMem(&si->transmem[SC_Lights], 1);
    l->type = LIG_POINT;
    l->d.point.color = (flo3){1, 1, 1};
    l->d.point.location = (flo3){-50, 100, 70};
    l->d.point.ls = 1;

    /*
    l = alloc_transMem(&si->transmem[SC_Lights], 1);
    l->type = LIG_POINT;
    l->d.point.color = (flo3){1, 1, 1};
    l->d.point.location = (flo3){-50, 50, -50};
    l->d.point.ls = 0.7;

    /*
     * Materials
     */
    Mat *m;
    m = alloc_transMem(&si->transmem[SC_Materials], 1);
    m->type = MAT_MATTE;
    m->d.matte.color = (flo3){0.99f, 0.0f, 0};
    m->d.matte.ambient_kd = 0.5;
    m->d.matte.direct_kd  = 0.5;

    
    m = alloc_transMem(&si->transmem[SC_Materials], 1);
    m->type = MAT_MATTE;
    m->d.matte.color = (flo3){0.1f, 0.8f, 0.1f};
    m->d.matte.ambient_kd = 0.5;
    m->d.matte.direct_kd  = 0.5;

    /*
     * Objects
     */


    for (int i = 0; i < 20; i++)
    {
        Obj *o = alloc_transMem(&si->transmem[SC_Objects], 1);
        o->type = OBJ_SPHERE;
        o->mat_id = i & 1;

        ObjSphere *sp = &o->d.sphere;
        sp->radius = 10;
        sp->pos = (flo3){
            rand() % 200 - 100,
            rand() % 20,
            -100-(rand() % 200), 
        };
    }
 

    for (int i = 0; i < s->gi.x; i++)
    for (int j = 0; j < s->gi.z; j++)
    {
        Obj *o = alloc_transMem(&si->transmem[SC_Objects], 1);
        o->type = OBJ_TRIANGLE;
        o->mat_id = i & 1;
        flo x  = s->gi.box.a.s[0];
        flo z  = s->gi.box.a.s[2];
        flo xd = s->gi.csize.s[0];
        flo yd = s->gi.csize.s[1];
        flo zd = s->gi.csize.s[2];
        set_tri(&o->d.tri,
                (flo3){x + xd * i,     -50, z + zd * (j + 1)},
                (flo3){x + xd * i,     -50, z + zd * j},
                (flo3){x + xd * (i+1) - 1, -50, z + zd * j});
    }

    /*
     * PLY models
     */

    Ply car;
    load_ply(&car, "models/car.ply");

    add_ply(si, 1, &car, mkflo3(15), (flo3){0, -20, 100});

}
