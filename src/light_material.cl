#ifndef __LIGHT_MATERIAL_CL_
#define __LIGHT_MATERIAL_CL_

#include "common.cl"
#include "hit.cl"
#include "grid.cl"

/*
 * BRDFs
 */
flo3 lambertian_rho(flo3 color, flo kd)
{
     return color * kd;
}

/*
 * Lights
 */
flo3 lig_ambient_l(LigAmbient *l)
{
     return l->color * l->ls;
}

flo3 lig_ambient_direction(LigAmbient *l, HitRec *hit)
{
     return (flo3)(0, 0, 0);
}

flo3 lig_point_l(LigPoint *l)
{
     return l->color * l->ls;
}

flo3 lig_point_direction(LigPoint *l, HitRec *hit)
{
     return normalize(l->location - hit->hit_point);
}


/*
 * Materials
 */
flo3 mat_shade_matte(MatMatte *m, Scene *s, HitRec *hit)
{
     flo3 wo = -hit->ray_d;
     flo3 out = (0);
     for (uint i = 0; i < s->num_ligs; i++)
     {
          if (s->lights[i].type == LIG_AMBIENT)
          {
               LigAmbient amb = s->lights[i].d.ambient;
               out += lambertian_rho(m->color, m->ambient_kd)
                    * lig_ambient_l(&amb);
          }
          else if (s->lights[i].type == LIG_POINT)
          {
               LigPoint poi = s->lights[i].d.point;
               flo3 wi = lig_point_direction(&poi, hit);
               flo ndotwi = dot(hit->normal, wi);
               if (ndotwi < 0) 
                    continue;
               Ray shadowRay = { .o = hit->hit_point, .d = wi };
               HitRec shadowHit = *hit;
               if (!HIT_FUNCTION(s, shadowRay, &shadowHit))
                    out += ndotwi * (lambertian_rho(m->color, m->direct_kd)
                              * lig_point_l(&poi));
          }
     }
     return out;
}

flo3 mat_shade(Scene *s, HitRec *hit)
{
     uint mat_id = s->objects[hit->obj_id].mat_id;
     Mat mat = s->materials[mat_id];
     switch (mat.type)
     {
     case MAT_MATTE: return mat_shade_matte(&mat.d.matte, s, hit);
     default: 
         //printf("Bad material %d on object #\n", mat.type);
         return (flo3)(0, 1, 1);
     }
}

    
// vim: ts=4 sw=5 et ft=c
#endif
