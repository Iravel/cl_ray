#ifndef __HIT_CL__
#define __HIT_CL__

#include "common.cl"

bool hit_plane(ObjPlane *o, Ray r, flo *t, flo3 *normal)
{
    *t = dot(o->pos - r.o, o->normal)
       / dot(r.d, o->normal);
    if (*t > EPS)
    {
        *normal = o->normal;
        return true;
    }
    return false;
}

bool hit_sphere(ObjSphere *o, Ray r, flo *t, flo3 *normal)
{
    flo3 temp = r.o - o->pos;
    flo a = dot(r.d, r.d);
    flo b = 2 * dot(temp, r.d);
    flo c = dot(temp, temp) - o->radius * o->radius;
    flo disc = b * b - 4 * a * c;

    if (disc < 0)
        return false;

    flo e = sqrt(disc);
    flo denom = 2 * a;
    flo dist = (-b - e) / denom;
    if (dist < EPS) 
        dist = (-b + e) / denom;

    if (dist > 1e-2f)
    {
        *t = dist;
        *normal = (temp + dist * r.d) / o->radius;
        return true;
    } else
        return false;
}

bool hit_triangle(ObjTriangle *o, Ray r, flo *t, flo3 *normal)
{
    flo a = o->x.s0 - o->x.s1, b = o->x.s0 - o->x.s2, c = r.d.x, d = o->x.s0 - r.o.x;
    flo e = o->y.s0 - o->y.s1, f = o->y.s0 - o->y.s2, g = r.d.y, h = o->y.s0 - r.o.y;
    flo i = o->z.s0 - o->z.s1, j = o->z.s0 - o->z.s2, k = r.d.z, l = o->z.s0 - r.o.z;
    
    flo m = f*k - g*j, n = h*k - g*l, p = f*l - h*j;
    flo q = g*i - e*k, s = e*j - f*i;

    flo inv_denom = 1.0f / (a*m + b*q + c*s);

    flo e1 = d*m - b*n - c*p;
    flo beta = e1 * inv_denom;

    if (beta < 0 || beta > 1)
        return false;

    flo rr = e*l - h*i;
    flo e2 = a*n + d*q + c*rr;
    flo gamma = e2 * inv_denom;

    if (gamma < 0 || gamma > 1)
        return false;

    if (beta + gamma > 1)
        return false;

    flo e3 = a*p - b*rr + d*s;
    flo tn = e3 * inv_denom;

    if (tn < 1e-2f)
        return false;

    *t = tn;
    *normal = (flo3)(o->x.s3, o->y.s3, o->z.s3);
    return true;
}

bool hit_obj(Obj *o, Ray r, flo *t, flo3 *normal)
{
    switch (o->type)
    {
    case OBJ_PLANE:    return hit_plane(&o->d.plane,   r, t, normal);
    case OBJ_SPHERE:   return hit_sphere(&o->d.sphere, r, t, normal);
    case OBJ_TRIANGLE: return hit_triangle(&o->d.tri, r, t, normal);
    default:
        printf("Bad type_id %u\n", o->type);
        return false;
    }
}


bool hit_objs(Scene *s, Ray r, HitRec *hit)
{
    flo t = 1e10f;
    flo cur_t;
    flo3 cur_normal;

    hit->ray_d = r.d;
    hit->obj_id = -1;

    for (uint i = 0; i < s->num_objs; i++)
    {
        Obj obj = s->objects[i];
        if (hit_obj(&obj, r, &cur_t, &cur_normal) && cur_t < t)
        {
            t = cur_t;
            hit->obj_id = i;
            hit->normal = cur_normal;
        }
    }

    if (hit->obj_id != -1)
    {
        hit->normal = normalize(hit->normal);
        hit->hit_point = r.o + t * r.d;
        return true;
    }
    return false;
}


// vim: ts=4 sw=4 et ft=c
#endif
