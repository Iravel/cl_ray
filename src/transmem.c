#include "transmem.h"

#include <assert.h>


void new_transMem(TransMem *tm, TransMemType type, size_t item_size)
{
    assert(item_size != 0);
    tm->mem  = NULL;
    tm->cmem = 0;
    tm->type = type;
    tm->used = 0;
    tm->count = 0;
    tm->changed = NULL;
    tm->allocated = 0;
    tm->item_size = item_size;
}

void delete_transMem(TransMem *tm)
{
    if (tm->mem == NULL) 
        return;

    free(tm->mem);
    clReleaseMemObject(tm->cmem);
    while (tm->changed != NULL)
    {
        void *cur = tm->changed;
        tm->changed = tm->changed->next;
        free(cur);
    }
}

void *alloc_transMem(TransMem *tm, size_t count)
{
    assert(tm->item_size != 0);
    assert(count > 0);
    if (tm->allocated < tm->used + tm->item_size * count)
    {
        size_t new_size_item = tm->used + tm->item_size * count;
        size_t new_size_alloc = tm->allocated * 2;
        size_t new_size = new_size_item > new_size_alloc ? new_size_item : new_size_alloc;
        assert(new_size > 0);
        tm->mem = realloc(tm->mem, new_size);
        assert(tm->mem != NULL);
        tm->allocated = new_size;
    }

    void *p = tm->mem + tm->used;
    tm->used += tm->item_size * count;
    tm->count += count;
    set_changed_region_transMem(tm, p, tm->item_size * count);
    return p;
}

void sync_to_cl_transMem(TransMem *tm, cl_context ctx, cl_command_queue cq)
{
    cl_int ret;
    if (tm->cmem == 0)
    {
        int mem_type;
        switch(tm->type)
        {
        case TM_RO: mem_type = CL_MEM_READ_ONLY;  break;
        case TM_WO: mem_type = CL_MEM_WRITE_ONLY; break;
        case TM_RW: mem_type = CL_MEM_READ_WRITE; break;
        }

        tm->cmem = clCreateBuffer(ctx, mem_type | CL_MEM_USE_HOST_PTR,
                tm->used, tm->mem, &ret);
        assert(ret == CL_SUCCESS);
    }

    if (!(tm->type & TM_RO))
        return;

    while (tm->changed != NULL)
    {
        ChangedMem *block = tm->changed;
        ret = clEnqueueWriteBuffer(cq, tm->cmem, CL_FALSE, 
            block->offset, block->size, tm->mem + block->offset, 0, NULL, NULL);
        assert(ret == CL_SUCCESS);
        tm->changed = block->next;
        free(block);
    }
}

void sync_from_cl_transMem(TransMem *tm, cl_context ctx, cl_command_queue cq)
{
    if (!(tm->type & TM_WO))
        return;

    cl_int ret = clEnqueueReadBuffer(cq, tm->cmem, CL_FALSE, 0, 
            tm->used, tm->mem, 0, NULL, NULL);

    assert(ret == CL_SUCCESS);
}

void set_changed_item_transMem(TransMem *tm, unsigned int number)
{
    assert(number * tm->item_size < tm->used);
    set_changed_region_transMem(tm, 
            tm->mem + number * tm->item_size, tm->item_size);
}

void set_changed_region_transMem(TransMem *tm, void *p, size_t size)
{
    char *c = p;
    assert(tm->mem <= c && c + size <= tm->mem + tm->used);
    ChangedMem *block = calloc(1, sizeof(ChangedMem));
    block->offset = c - tm->mem;
    block->size   = size;
    block->next   = tm->changed;
    tm->changed   = block;
}

