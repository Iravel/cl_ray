#ifndef __GRID_CL_
#define __GRID_CL_

#include "common.cl"
#include "hit.cl"


int in_bbox(flo3 p, BBox box)
{ return all(box.a <= p && p <= box.b); }

flo my_min_inf(flo a, flo b)
{ return a > -1e8f && a < b ? a : b; }

flo my_max_inf(flo a, flo b)
{ return a < 1e8f && a > b ? a : b; }

bool hit_bbox(Ray r, BBox box, flo *tmin)
{
    //~~ p. 360

    flo3 inv_d = 1 / r.d;
    flo2 tx, ty, tz;

    tx = (flo2)(box.a.x, box.b.x);
    ty = (flo2)(box.a.y, box.b.y);
    tz = (flo2)(box.a.z, box.b.z);

    if (inv_d.x < 0) tx = tx.yx;
    if (inv_d.y < 0) ty = ty.yx;
    if (inv_d.z < 0) tz = tz.yx;

    tx = (tx - r.o.x) * inv_d.x;
    ty = (ty - r.o.y) * inv_d.y;
    tz = (tz - r.o.z) * inv_d.z;

    flo2 t;
    t.x = fmax(tx.x, fmax(ty.x, tz.x));
    t.y = fmin(tx.y, fmin(ty.y, tz.y));

    if (t.x > t.y || t.y < EPS)
        return false;

    *tmin = (t.x > EPS ? t.x : t.y);
    return true;
}

bool hit_grid_cell(uint cell_id, Scene *s, Ray r, flo *t, HitRec *hit)
{
    flo cur_t;
    flo3 cur_normal;
    hit->obj_id = -1;
    uint row_id = s->grid[cell_id];
    *t = 1e9f;
    while (0 != row_id)
    {
        for (int i = 1; i < GRID_ROW_SIZE; i++)
        {
            uint obj_id = GRID_ROW(s->grid_rows, row_id, i);
            if (0 == obj_id) 
                continue;

            obj_id--;

            Obj obj = s->objects[obj_id];
            if (hit_obj(&obj, r, &cur_t, &cur_normal) && cur_t < *t)
            {
                *t = cur_t;
                hit->obj_id = obj_id;
                hit->normal = cur_normal;
            }
        }
        row_id = GRID_ROW(s->grid_rows, row_id, 0);
    }

    if (-1 != hit->obj_id)
    {
        hit->normal = normalize(hit->normal);
        hit->hit_point = r.o + (*t) * r.d;
        return true;
    }
    return false;
}

bool hit_grid(Scene *s, Ray r, HitRec *hit)
{
    // p. 455
    flo3 p;
    if (in_bbox(r.o, s->gi.box))
        p = r.o;
    else
    {
        flo t0;
        if (!hit_bbox(r, s->gi.box, &t0))
            return false;

        p = r.o + t0 * r.d;
    }

    int3 grid_size = (int3)(s->gi.x, s->gi.y, s->gi.z);
    int3 pos = 
    {
        clamp(GET_GRID_X(p, s->gi), 0, grid_size.x - 1),
        clamp(GET_GRID_Y(p, s->gi), 0, grid_size.y - 1),
        clamp(GET_GRID_Z(p, s->gi), 0, grid_size.z - 1),
    };

    int3 sign =
    {
        r.d.x < -EPS ? -1 : (r.d.x > EPS ? +1 : 0),
        r.d.y < -EPS ? -1 : (r.d.y > EPS ? +1 : 0),
        r.d.z < -EPS ? -1 : (r.d.z > EPS ? +1 : 0),
    };
    int3 sign2 = clamp(sign, 0, 1);

    flo3 t_next = s->gi.box.a + convert_float3(pos + sign2) * s->gi.csize;   
    t_next -= r.o;
    t_next /= r.d;

    flo3 step = fabs(s->gi.csize / r.d);

    while (true)
    {
        uint cell_id = GET_GRID_CELL_(pos.x, pos.y, pos.z, s->gi);
        flo  t;
        bool hit_bool;
        hit_bool = hit_grid_cell(cell_id, s, r, &t, hit);

        if ( sign.x != 0 && 
            (sign.y == 0 || t_next.x < t_next.y) &&
            (sign.z == 0 || t_next.x < t_next.z))
        {
            if (hit_bool && t < t_next.x)
                return true;

            pos.x += sign.x;
            t_next.x += step.x;

            if (pos.x == grid_size.x || pos.x == -1)
                return false;

            continue; // to the next cell
        }

        if ( sign.y != 0 && 
            (sign.z == 0 || t_next.y < t_next.z))
        {
            if (hit_bool && t < t_next.y)
                return true;

            pos.y += sign.y;
            t_next.y += step.y;

            if (pos.y == grid_size.y || pos.y == -1)
                return false;

            continue; // to the next cell
        }

        if (sign.z != 0)
        {
            if (hit_bool && t < t_next.z)
                return true;

            pos.z += sign.z;
            t_next.z += step.z;

            if (pos.z == grid_size.z || pos.z == -1)
                return false;
         
            continue; // to the next cell
        }
        // no movements
        return false;
    }
}

#endif
// vim: ft=c
