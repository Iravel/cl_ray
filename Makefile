
ray_list = $(shell find src -name "*.c" | sort -r | sed 's/^src\///;s/.c$$//')

ray_objs = $(foreach file,$(ray_list), bin/$(file).o)

libs = sdl2

CPPINCLUDE = $(shell pkg-config --cflags $(libs)) \
			 -I/opt/AMDAPP/include \
			 -Iinclude -Isrc
		
CFLAGS = -g -std=c11 -Ofast -flto -Wall -march=native -mtune=native -Wno-missing-braces

mLDFLAGS   = $(shell pkg-config --libs $(libs)) \
			 -lOpenCL -L/opt/AMDAPP/lib/x86_64/lib \
			 -lm

COMPILE = $(CC) -c $(CFLAGS) $(CPPFLAGS) $(CPPINCLUDE)
LINK = $(CC) $(CFLAGS) 
LINK_E = $(mLDFLAGS)

.PHONY: all
all: show ray compiled.cl

ray: bin/ray
	@echo -n " [CP ] Copy to root... "
	@cp bin/ray ray
	@echo "ok"

bin/%.o: src/%.c
	@mkdir -p $(shell dirname $@)
	@echo -n " [CC ] Compiling $<... \t"
	@$(COMPILE) $< -o $@
	@echo "ok"

.SECONDARY: bin/ray
bin/ray: $(ray_objs)
	@echo -n " [LD ] Linking all together in $@... "
	@$(LINK) $^ -o $@ $(LINK_E)
	@echo "ok"

.PHONY: clean
clean:
	@echo -n " [RM ] Cleaing all... "
	@$(RM) -R bin/* ray
	@echo "ok"

.PHONY: show
show:
	@echo 
	@echo "Showing commands"
	@echo "\tCOMPILE = $(COMPILE)"
	@echo "\tLINK    = $(LINK)"
	@echo "\tLINK_E  = $(LINK_E)"
	@echo 

.PHONY: run
run: ray
	@echo "\nRunning ray"
	@bash -c "time ./ray"

compiled.cl: src/*.cl include/shared.h
	cpp -undef -Iinclude src/render.cl $@

