#ifndef __C_MATH__
#define __C_MATH__

#include "shared.h"
#include <math.h>

#define C_VFOR(c) for (uint i = 0; i < (c); i++)

inline flo2 mkflo2(flo a)
{
    return (flo2){a, a};
}

inline flo3 mkflo3(flo a)
{
    return (flo3){a, a, a};
}

inline flo4 mkflo4(flo a)
{
    return (flo4){a, a, a};
}

#define C_FUN(name, arity, ...)     \
    inline void                     \
    name##arity(__VA_ARGS__)        \
    {                               \

#define C_FUNE                      \
    }


#define SIMPLE2_(name, arity, op)   \
    C_FUN(name, arity,              \
            flo##arity *a,          \
            flo##arity *b)          \
        C_VFOR(arity)               \
            a->s[i] =               \
                a->s[i] op (b->s[i]);\
    C_FUNE

#define SIMPLE2(name, op)           \
    SIMPLE2_(name, 2, op)           \
    SIMPLE2_(name, 3, op)           \
    SIMPLE2_(name, 4, op)

SIMPLE2(add, +)
SIMPLE2(mul, *)
SIMPLE2(sub, -)
SIMPLE2(div, /)

#define UNARY_(name, arity, op)     \
    C_FUN(name, arity, flo##arity *a) \
        C_VFOR(arity)               \
            a->s[i] = op (a->s[i]); \
    C_FUNE

#define UNARY(name, op)             \
    UNARY_(name, 2, op)             \
    UNARY_(name, 3, op)             \
    UNARY_(name, 4, op)

UNARY(inv, 1/)
UNARY(neg, -)

#define SUM(arity)                  \
    inline flo                      \
    sum##arity(flo##arity *a)       \
    {                               \
        flo sum = 0;                \
        C_VFOR(arity)               \
            sum += a->s[i];         \
        return sum;                 \
    }

SUM(2)
SUM(3)
SUM(4)

#define SQR(arity)                  \
    inline flo                      \
    sqr##arity(flo##arity *a)       \
    {                               \
        flo sum = 0;                \
        C_VFOR(arity)               \
            sum += a->s[i]*a->s[i]; \
        return sum;                 \
    }

SQR(2)
SQR(3)
SQR(4)

#define LEN(arity)                  \
    inline flo                      \
    len##arity(flo##arity *a)       \
    {                               \
        return sqrt(sqr##arity(a)); \
    }

LEN(2)
LEN(3)
LEN(4)


#define NORMALIZE(arity)            \
    inline flo                      \
    normalize##arity(flo##arity *a) \
    {                               \
        flo len = len##arity(a);    \
        flo##arity b = mkflo##arity(1/len); \
        mul##arity(a, &b);          \
        return len;                 \
    }

NORMALIZE(2)
NORMALIZE(3)
NORMALIZE(4)

#define CMP_(name, op, arity)       \
    inline int                      \
    name##arity(flo##arity *a,      \
            flo##arity *b)          \
    {                               \
        C_VFOR(arity)               \
            if (!(a->s[i] op b->s[i]))\
                return 0;           \
        return 1;                   \
    }

#define CMP(name, op)               \
        CMP_(name, op, 2)           \
        CMP_(name, op, 3)           \
        CMP_(name, op, 4)            

CMP(isless,      <)
CMP(islesseq,    <=)
CMP(isgreater,   >)
CMP(isgreatereq, >=)


#define FOLD_(name, fun, arity)     \
    inline flo                      \
    name##arity(flo##arity *a)      \
    {                               \
        flo acc = a->s[0];          \
        for (int i = 1; i < arity; i++) \
            acc = fun(acc, a->s[i]); \
        return acc;                 \
    }
            
#define FOLD(name, fun)             \
        FOLD_(name, fun, 2)         \
        FOLD_(name, fun, 3)         \
        FOLD_(name, fun, 4)

inline flo _my_min(flo a, flo b) { return a < b ? a : b; }
inline flo _my_max(flo a, flo b) { return a > b ? a : b; }
FOLD(min, _my_min)
FOLD(max, _my_max)


inline flo _my_abs(flo a)        { return a > 0 ? a : -a; }
inline int near(flo a, flo b)    { return _my_abs(a - b) < 0.01; }
inline int absnear(flo a, flo b) { return near(_my_abs(a), _my_abs(b)); }

#define MAP(dst, fun, arity, src)   \
    C_VFOR(arity)                   \
        dst[i] = fun(src[i])

inline flo2 rotate2(flo2 a, flo angle)
{
    return (flo2)
    {
        a.s[0] * cosf(angle) - a.s[1] * sinf(angle),
        a.s[0] * sinf(angle) + a.s[1] * cosf(angle),
    };
}





#endif

