#ifndef __WORLD_h_
#define __WORLD_h_

#include "shared.h"
#include "transmem.h"

typedef enum
{
    SC_Objects   = 0,
    SC_Lights    = 1,
    SC_Materials = 2,
    SC_Grid      = 3,
    SC_GridRows  = 4,
    SC_Pixels    = 5,

    SC_LastOne
} SceneMemoryRegions;


typedef struct
{
    Scene       scene;
    TransMem    transmem[SC_LastOne];
} SceneInfo;

typedef struct
{
    flo3 *vertexes;
    int v_count;

    int **faces;
    int f_count;
} Ply;

void make_scene(SceneInfo *si);


#endif

