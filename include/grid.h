#ifndef __GRID_H_
#define __GRID_H_

#include "shared.h"
#include "transmem.h"

typedef struct
{
    Grid        *g;
    GridRow     *rows;

    TransMem    *grid_tm;
    TransMem    *rows_tm;

    GridInfo    gi;
    uint        r_count, r_last;
} GridAlloc;


void
new_grid(GridAlloc *ga, flo3 a, flo3 b, flo3 dens);

void
delete_grid(GridAlloc *ga);

void
insert_objs_into_grid(GridAlloc *ga, Obj *objs, uint num);

#endif

