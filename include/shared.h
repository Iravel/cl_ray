#ifndef __SHARED__
#define __SHARED__

#ifdef __GNUC__

#   include "CL/cl.h"
#   define GLOBAL 
#   define ALIGNED(bytes) __attribute__ ((aligned(bytes)))
    typedef cl_float flo   ALIGNED(4);

    typedef cl_float2 flo2 ALIGNED(8);
    typedef cl_float3 flo3 ALIGNED(16);
    typedef cl_float4 flo4 ALIGNED(16);

    typedef cl_uint pixel;
    typedef cl_uint uint;

#   define _s_0 s[0]
#   define _s_1 s[1]
#   define _s_2 s[2]
#   define _s_3 s[3]

#else

#   define GLOBAL __global
    typedef float flo;

    typedef float2 flo2;
    typedef float3 flo3;
    typedef float4 flo4;

    typedef uint pixel;
    typedef int  cl_int;
    typedef uint cl_uint;
    typedef void *cl_mem;

#   define _s_0 s0
#   define _s_1 s1
#   define _s_2 s2
#   define _s_3 s3

#endif

typedef pixel *pixels;

typedef struct
{
    flo3 o;
    flo3 d;
} Ray;

typedef struct
{
    flo3 a, b;
} BBox;

typedef struct
{
    flo3 color;
    flo  kd;
} BrdfLambertian;


/*
 * Light
 */
typedef enum
{
    LIG_NONE = 0,
    LIG_AMBIENT = 1,
    LIG_POINT = 2,
    LIG_TYPE_LAST
} LigType;

typedef struct
{
    flo3 color;
    flo ls;
} LigAmbient;

typedef struct
{
    flo3 color;
    flo3 location;
    flo  ls;
} LigPoint;

typedef struct
{
    LigType type;
    union
    {
        LigAmbient ambient;
        LigPoint   point;
    } d;
} Lig;

/*
 * Materials
 */
typedef enum
{
    MAT_NONE = 0,
    MAT_MATTE = 1,
    MAT_TYPE_LAST
} MatType;

typedef struct
{
    flo3 color;
    flo ambient_kd, direct_kd;
} MatMatte;

typedef struct
{
    MatType type;
    union
    {
        MatMatte matte;
    } d;
} Mat;

/*
 * Objects
 */
typedef enum
{
    OBJ_NONE = 0,

    OBJ_PLANE = 1,
    OBJ_SPHERE = 2,
    OBJ_TRIANGLE = 3,

    OBJ_TYPE_LAST
} ObjType;

typedef struct
{
    flo3 pos;
    flo3 normal;
} ObjPlane;

typedef struct
{
    flo3 pos;
    flo  radius;
} ObjSphere;

typedef struct
{
    flo4 x, y, z; // a, b, c, normal
} ObjTriangle;


typedef struct
{
    ObjType type;
    int mat_id;
    union 
    {
        ObjSphere   sphere;
        ObjPlane    plane;
        ObjTriangle tri;
    } d;
} Obj;

#define GRID_ROW_SIZE 4
#define GRID_ROW(g, i, j) g[i * GRID_ROW_SIZE + j]
typedef uint GridRow;
typedef uint Grid;

typedef struct 
{
    BBox box;
    flo3 csize;   // cell size
    uint x, y, z; // grid size (cells count)
} GridInfo;

#define GET_GRID_X(p, gi)   (int)(floor(((p)._s_0 - (gi).box.a._s_0) / (gi).csize._s_0))
#define GET_GRID_Y(p, gi)   (int)(floor(((p)._s_1 - (gi).box.a._s_1) / (gi).csize._s_1))
#define GET_GRID_Z(p, gi)   (int)(floor(((p)._s_2 - (gi).box.a._s_2) / (gi).csize._s_2))

#define GET_GRID_CELL_(x_, y_, z_, gi) ((x_) + gi.x * ((y_) + gi.y * (z_)))

#define GET_GRID_CELL(p, gi)                        \
    GET_GRID_CELL_(GET_GRID_X(p, gi),               \
                   GET_GRID_Y(p, gi),               \
                   GET_GRID_Z(p, gi),               \
                   gi)


/*
 * Scene
 */
typedef struct
{
    GLOBAL Lig     *lights;
    GLOBAL Mat     *materials;
    GLOBAL Obj     *objects;
    GLOBAL Grid    *grid;
    GLOBAL GridRow *grid_rows;

    uint num_ligs;
    uint num_mats;
    uint num_objs;
    uint num_cells;
    uint num_rows;

    GridInfo gi;

    flo2 view_angle;
    Ray base_ray;
} Scene;

#define SHOW(fmt, ...)  printf(#__VA_ARGS__ ": " fmt " " , __VA_ARGS__)
#define SHOWn(fmt, ...) printf(#__VA_ARGS__ ": " fmt "\n", __VA_ARGS__)
#define SHOW_flo3n(v) printf(#v ": (%f %f %f)\n",                              \
        (v)._s_0, (v)._s_1, (v)._s_2);
#define SHOW_int3n(v) printf(#v ": (%d %d %d)\n",                              \
        (v)._s_0, (v)._s_1, (v)._s_2);

#define GRID_DEBUG(x) x

#endif
