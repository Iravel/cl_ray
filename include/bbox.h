#ifndef __BBOX_H_
#define __BBOX_H_

#include "shared.h"
#include "c_math.h"
#include <stdio.h>

inline void
new_bbox(BBox *box, flo3 a, flo3 b)
{
    if (islesseq3(&a, &b))
    {
        box->a = a;
        box->b = b;
    }
    else if (islesseq3(&b, &a))
    {
        box->a = b;
        box->b = a;
    }
}


inline void
tri_bbox(ObjTriangle *obj, BBox *box)
{
    box->a.s[0] = min3(&obj->x);
    box->a.s[1] = min3(&obj->y);
    box->a.s[2] = min3(&obj->z);

    box->b.s[0] = max3(&obj->x);
    box->b.s[1] = max3(&obj->y);
    box->b.s[2] = max3(&obj->z);
}

inline void
sphere_bbox(ObjSphere *obj, BBox *box)
{
    box->a = box->b = obj->pos;
    flo3 radius = mkflo3(obj->radius);
    sub3(&box->a, &radius);
    add3(&box->b, &radius);
}


inline void
plane_bbox(ObjPlane *obj, BBox *box)
{
    flo3 r;
    if (absnear(obj->normal.s[0], 1) &&
        absnear(obj->normal.s[1], 0) &&
        absnear(obj->normal.s[2], 0))
        r = (flo3){0, 100, 100};
    else if (absnear(obj->normal.s[0], 0) &&
             absnear(obj->normal.s[1], 1) &&
             absnear(obj->normal.s[2], 0))
        r = (flo3){100, 0, 100};
    else if (absnear(obj->normal.s[0], 0) &&
             absnear(obj->normal.s[1], 0) &&
             absnear(obj->normal.s[2], 1))
        r = (flo3){100, 100, 0};

    box->a = box->b = obj->pos;
    sub3(&box->a, &r);
    add3(&box->b, &r);
}


#endif
