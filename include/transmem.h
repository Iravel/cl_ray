#ifndef __TRANSMEM_H_
#define __TRANSMEM_H_

#include "CL/cl.h"

struct ChangedMem_s;
typedef struct 
{
    size_t      offset;
    size_t      size;
    struct ChangedMem_s  *next;
} ChangedMem;


typedef enum
{
    TM_RO = 1,
    TM_WO = 2,
    TM_RW = 4
} TransMemType;

typedef struct
{
    char        *mem;
    cl_mem      cmem;

    size_t      used;
    size_t      allocated;
    unsigned int count;

    TransMemType type;
    size_t      item_size;

    ChangedMem  *changed;
} TransMem;

void new_transMem(TransMem *tm, TransMemType type, size_t item_size);
void delete_transMem(TransMem *tm);
void *alloc_transMem(TransMem *tm, size_t count);
void sync_to_cl_transMem(TransMem *tm, cl_context ctx, cl_command_queue cq);
void sync_from_cl_transMem(TransMem *tm, cl_context ctx, cl_command_queue cq);
void set_changed_item_transMem(TransMem *tm, unsigned int number);
void set_changed_region_transMem(TransMem *tm, void *p, size_t size);


#endif

